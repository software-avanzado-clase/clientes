<?php

require 'vendor/autoload.php';

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    public function helloWorld()
    {
        $this->say(SA\Shared\Shared::saludar());
    }

    public function saludarYDespedir()
    {
        $this->say(SA\Shared\Shared::saludar('a todos los alumnos'));
        $this->say(SA\Shared\Shared::retornarDespedida());
    }
}
